'''
Created on Sep 8, 2016

@author: whitesabertooth
'''
#################################
#  I M P O R T S
#################################
import numpy as np
import pickle
import time
from bptt import mymodel
import sys
import os
import subprocess
from tabulate import tabulate
import threading

## GLOBAL CONST
'''ADD Progress bar
def update_progress(progress):
    print '\r[{0}] {1}%'.format('#'*(progress/10), progress)
looks like this: [ ########## ] 100%
'''
score_offset = 300 #worst possible score...this is a guess; added to score to make base of 0
save_best_net_file = 'net.dump'
save_best_log_file = 'log.txt'
save_best_dist_file = 'dist.txt'
save_best_analysis_file = 'analysis.txt'
save_dir = "data"
CONST_SAVE_NETWORK_EVERY_ITER = 10 # Every certain amount of iterations to skip before saving the network


def evaluate(mygenome,num_scenarios,dolearn,record,iteration,prefix):
    fitness = 0
    sys.stdout.write(" Running %d scenarios"%(num_scenarios))
    proc = subprocess.Popen(['./run.sh', '%d'%(num_scenarios)],stdin=subprocess.PIPE,stdout=subprocess.PIPE,universal_newlines=True)#universal_newlines=True makes the PIPEs str instead of byte
    notend = True
    scenario_num = -1
    allfitness = []
    while notend:
        line = proc.stdout.readline()
        #print(line)
        if line[0] == 's':#final average score
            notend = False
            fitness = float(line.split(":")[1])
        elif line[0] == 'i':#print intermediate score
            scenario_num +=1 #increment scenario
            #print(line)
            sys.stdout.write(".")
            sys.stdout.flush()
            #learn
            tmpfitness = float(line.split(":")[1])
            allfitness.append(tmpfitness) #save
            if dolearn:
                mygenome.add_to_learning(tmpfitness)#save learning for later
            mygenome.reset() #clear old scenario
            pass
        else:
            inpercepts = int(line)
            #split the percepts by 2^(0-4)
            nnpercept = [] #bump,glitter,breeze,stench,scream
            for ii in range(5):
                nnpercept.append( ( inpercepts & (2**ii) ) / (2**ii) )
            nnpercept = np.array(nnpercept)
            action = mygenome.predict(nnpercept)
            #print("in: ",nnpercept,", out: %d"%(action))
            proc.stdin.write(str(action)+"\n")
            if proc.returncode == None:
                proc.stdin.flush()
            else:
                print("terminated too early")
                notend = False
    proc.terminate() #should be done anyway, but just to make sure
    bestarr = np.array(allfitness)
    ltone = (bestarr < -1000).sum()
    gtone = (bestarr > 500).sum()
    other = args.scenarios - ltone - gtone
    sys.stdout.write("Avg %.2f, Rng: - %d / %d / + %d"%(fitness,ltone,other,gtone))
    sys.stdout.flush()
    return (fitness,allfitness)

def doTraining(model=None,tolearn=True,num_scenarios=1000,iteration=0,prefix=''):
    #Set parameters based on vizdoom
    #print("Starting the training!")
    score = evaluate(model,num_scenarios,tolearn,False,iteration,prefix)
    if tolearn:
        model.learn()#actually perform all the learning
    sys.stdout.write("\n")
    #Finished Learning
    return (score,model)

def saveNet(net,filename=save_best_net_file):
    sys.stdout.write("Saving the best neural network to %s..." %(filename))
    sys.stdout.flush()
    net.save(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()

def loadNet(filename=save_best_net_file,dolearn=True):
    sys.stdout.write("Loading neural network from %s..." %(filename))
    sys.stdout.flush()
    net = mymodel({},filename,tolearn=dolearn)
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net



if __name__ == '__main__':
    import argparse
    #=======================
    # Process arguments
    parser = argparse.ArgumentParser(description='Demo NEAT.')
    parser.add_argument('--load_net', dest='net', metavar='FILE',
                        help='File of a dumped neural network')
    parser.add_argument('--prefix', dest='prefix', metavar='PREFIX',
                        help='Prefix to add to all dumped files')
    parser.add_argument('--config', dest='config', metavar='CONFIG',
                        help='Configuration file')
    parser.add_argument('--scenarios', dest='scenarios', type=int, metavar='NUM',
                        help='Number of scenarios to run', default=1000)
    parser.add_argument('--learn', dest='learn', action='store_true', default=False,
                        help='Make it learn while executing')
    parser.add_argument('--save_net', dest='save', action='store_true', default=False,
                        help='Save net even if not learning')
    parser.add_argument('--iterations', dest='iterations', type=int, metavar='NUM',
                        help='Number of scenarios to run', default=1)
    parser.add_argument('--last_iter', dest='last_iter', type=int, metavar='NUM',
                        help='Last Iteration', default=-1)
    parser.add_argument('--net_info', dest='net_info', action='store_true', default=False,
                        help='Show neural network probablity distribution information.')
    parser.add_argument('--print', dest='doprint', action='store_true', default=False,
                        help='Print neural network weights.')
    prefix = "best"
    model = None
    args = parser.parse_args()
    if args.config:
        #load config file
        from ConfigParser import SafeConfigParser
        parser = SafeConfigParser()
        parser.read(args.config)

    if args.prefix:
        prefix = args.prefix
    if args.learn:
            print("Using learning")

    #If net set then just show demo
    if args.net:
        model = loadNet(args.net,args.learn)
        print("Using neural network loaded")
        if args.doprint:
            model.print_weights(sys.stdout)
            exit(0)

    if args.net_info:
        if net == None:
            print("Cannot determing net info without loading a net")
            exit(1)
        print(net.net_info())
        exit(0)

    if not model:
        model = mymodel({},tolearn=args.learn)

    #Create save directory
    predir = prefix+save_dir
    if not os.path.exists(predir):
        os.mkdir(prefix+save_dir)
    predir += "/"

    #Only save initial distribution if first iteration
    if args.last_iter == -1 and (args.learn or args.save):
        fname = predir + prefix+'_s_'+save_best_dist_file
        with open(fname,'w') as f:
            f.write(model.net_info())
        print("Initial Saved distribution to %s" %(fname))


    time_start = time.time()
    #Start from where left off
    for ii in range(args.last_iter+1,args.iterations+args.last_iter+1):
        #=======================
        # Do training
        model.reset() #do a reset before starting a training
        #print("+========================+")
        sys.stdout.write("| Starting iteration %d:"%(ii))
        (best,model) = doTraining(model,args.learn,args.scenarios,ii,prefix)
        #print("| Average score before training: %.2f"%(best[0]))
        #print("+=====================================+")

        #=======================
        # Save data
        if args.save or args.learn:
            #print("======================================")
            import os
            #model.reset() #do not reset before this prediction
            #Do this before predictions
            if args.learn and model.isLogging():
                fname = predir+prefix+'_%d_'%(ii)+save_best_analysis_file
                with open(fname,'w') as f:
                    f.write("Score %.2f\n"%(best[0]))
                    model.learn_analysis(f)
                #print("Saved analysis to %s" %(fname))

            #Save every certain iteration, but not the first (+1)
            if ((ii+1) % CONST_SAVE_NETWORK_EVERY_ITER) == 0:
                #Save network
                fname = predir+prefix+save_best_net_file
                if os.path.exists(fname):
                    os.remove(fname)
                saveNet(model,fname)

            fname = predir+prefix+save_best_log_file
            if not os.path.exists(fname):
                with open(fname,'w') as f:
                    #write header
                    f.write("Iter\tAvg\t<-1000\t>500\tBtwn\t")
                    for dd in range(args.scenarios):
                        f.write("S%d\t"%(dd+1))
                    f.write("\n")
            with open(fname,'a') as f:
                #write
                #best[1].insert(0,best[0]) #add average to begining
                f.write('%d\t'%ii)
                f.write('%.2f\t'%best[0])
                bestarr = np.array(best[1])
                ltone = (bestarr < -1000).sum()
                gtone = (bestarr > 500).sum()
                other = args.scenarios - ltone - gtone
                f.write('%d\t'%( ltone ))
                f.write('%d\t'%( gtone ))
                f.write('%d\t'%( other ))
                for num in best[1]:
                    f.write('%.2f\t'%num)
                f.write("\n")
            #print("Appended to log %s" %(fname))

            fname = predir+prefix+'_%d_'%(ii)+save_best_dist_file
            with open(fname,'w') as f:
                f.write(model.net_info())
            #print("Saved distribution to %s" %(fname))

    if args.save or args.learn:
        #ALWAYS save on last time
        #Save network
        fname = predir+prefix+save_best_net_file
        if os.path.exists(fname):
            os.remove(fname)
        saveNet(model,fname)

    print("Total elapsed time: %.2f minutes" % ((time.time() - time_start) / 60.0))
