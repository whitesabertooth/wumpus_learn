import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import pylab

class neuron():
    def __init__(self):
        self.m_a = None
        self.m_b = None
        self.m_timeconst = None
        self.m_bias = None
        self.m_split_y = None
        self.m_type = None
        self.m_activation_function_type = None
        self.next = []
        self.idx = None

        pass

class connection():
    def __init__(self):
        self.m_source_neuron_idx = None
        self.m_target_neuron_idx = None
        self.m_weight = None

def Load(a_DataFile):
    t_str = ""
    t_no_start = True
    t_no_end = True

    if not a_DataFile:
        print("NN file error!");

    # search for NNstart
    while t_str != "NNstart" and t_str != None:
        t_str = a_DataFile.readline().strip()
        if (t_str == "NNstart"):
            t_no_start = False

    if (t_no_start):
        return ([],[])

    # read in the input/output dimentions
    t_str = a_DataFile.readline().strip()
    tmparr = t_str.split(" ")
    m_num_inputs = int(tmparr[0])
    m_num_outputs= int(tmparr[1])

    m_neurons = []
    m_connections = []
    curnum = 0
    # read in all data
    while t_str != "NNend" and t_str != None:
        t_str = a_DataFile.readline().strip()

        # a neuron?
        if t_str.startswith("neuron"):
            # for type and aftype
            t_n = neuron()
            tmparr = t_str.split(" ")

            t_n.m_type = int(tmparr[1])
            t_n.m_a = float(tmparr[2])

            t_n.m_b = float(tmparr[3])
            t_n.m_timeconst = float(tmparr[4])
            t_n.m_bias = float(tmparr[5])
            t_n.m_activation_function_type  = int(tmparr[6])
            t_n.m_split_y = float(tmparr[7])
            t_n.idx = curnum
            curnum += 1
            m_neurons.append(t_n)

        # a connection?
        if t_str.startswith("connection"):
            t_c = connection()
            tmparr = t_str.split(" ")
            t_c.m_source_neuron_idx = int(tmparr[1])
            t_c.m_target_neuron_idx = int(tmparr[2])
            t_c.m_weight = float(tmparr[3])
            #a_DataFile >> t_isrecur;

            #a_DataFile >> t_c.m_hebb_rate;
            #a_DataFile >> t_c.m_hebb_pre_rate;

            #t_c.m_recur_flag = static_cast<bool>(t_isrecur);

            m_connections.append(t_c)



        if (t_str == "NNend"):
            t_no_end = False

    if (t_no_end):
        print("NNend not found in file!")

    return (m_neurons,m_connections)

def getname(neuron,input_size):
    arr = ["bias","bump","glitter","breeze","stench","scream"]
    act = ["FOWARD","RIGHT","LEFT","GRAB","SHOOT","NOOP"]
    if neuron.idx >= input_size and neuron.idx < (input_size+len(act)):
        return act[neuron.idx-input_size]
    elif neuron.idx < input_size:
        return "%s_%d" % (arr[neuron.idx % 6],int(neuron.idx/6))
    else:
        return "H_%d" % (neuron.idx - input_size - len(act))

G = nx.DiGraph()
import argparse
from tabulate import tabulate
#=======================
# Process arguments
parser = argparse.ArgumentParser(description='Demo NEAT.')
parser.add_argument(dest='net', metavar='FILE',
                    help='File of a dumped network')
parser.add_argument('--show_graph', dest='show_graph', action='store_true', default=False,
                    help='Print the graph')
parser.add_argument('--input_size', dest='input_size', metavar='NUM', type=int, default=1, help='Number of input memory (sets of 6)')
args = parser.parse_args()

#Set input size
input_size = args.input_size * 6

#Load file
with open(args.net,'r') as f:
    (neurons,conns) = Load(f)

#Setup table to be blank
tbl = [ [ 0.0 for _ in range(len(neurons)+1) ] for _ in range(len(neurons))]#  [[0.0]*(len(neurons)+1)]*(len(neurons))

#Show connection weights
for conn in conns:
    nodebtxt = getname(neurons[conn.m_target_neuron_idx],input_size)
    nodeatxt = getname(neurons[conn.m_source_neuron_idx],input_size)
    G.add_edges_from([(nodeatxt,nodebtxt)],weight=conn.m_weight)
    #Look for recurrent on same node
    if conn.m_target_neuron_idx == conn.m_source_neuron_idx:
        post_str = "*"
    else:
        post_str = ""
    tbl[conn.m_source_neuron_idx][conn.m_target_neuron_idx+1] = '%s%.5f%s'%(post_str,conn.m_weight,post_str)
    #map node to next one
    neurons[conn.m_source_neuron_idx].next.append(conn.m_target_neuron_idx)

#Determine node position
nodes_pos = {}
nodes_label = {}
layers = []
layers_name = []
newlayer = []
newlayer_name = []
hdr = []
for neuron in neurons:
    #setup table based on order of nodes
    node_name = getname(neuron,input_size)
    tbl[neuron.idx][0] = node_name
    hdr.append(node_name)
    #find the output layer (has not next node)
    if node_name[0].isupper() and node_name[0] != 'H': #len(neuron.next) == 0:
        newlayer.append(neuron.idx)
        newlayer_name.append(node_name)
#add output layer
layers.append(newlayer)
layers_name.append(newlayer_name)

#add all hidden layers
#Start from the end layer and work backwards
hidden_layer_added = []
while len(newlayer) > 0:
    do_once_more = False
    newlayer = []
    newlayer_name = []
    for neuron in neurons:
        if neuron.idx not in hidden_layer_added:
            node_name = getname(neuron,input_size)
            foundidx = False
            #search for hidden layers that is next layer
            if node_name[0] == 'H':
                for inneridx in layers[0]:
                    if inneridx in neuron.next:
                        foundidx = True
                        break

            if foundidx:
                hidden_layer_added.append(neuron.idx)
                newlayer.append(neuron.idx)
                newlayer_name.append(node_name)
    if len(newlayer) > 0:
        layers.insert(0,newlayer) #inset at head
        layers_name.insert(0,newlayer_name)

#Add the input layer
newlayer = []
newlayer_name = []
for neuron in neurons:
    node_name = getname(neuron,input_size)
    if node_name[0] != 'H' and node_name[0].islower():
        newlayer.append(neuron.idx)
        newlayer_name.append(node_name)
layers.insert(0,newlayer) #inset at head
layers_name.insert(0,newlayer_name)

#TODO: Add all other nodes that have no connections
print("Weights for connection from Neuron on Row --> Neuron on Column\n")
print(tabulate(tbl,headers=hdr))



if args.show_graph:
    # add the units per layer
    for layer_i in range(len(layers)):
        is_input = layer_i == 0
        is_output = layer_i == len(layers) -1
        layer_size = len(layers[layer_i])
        count = 0
        # add the nodes for this layer
        for node_i in range(layer_size):
            node = getname(neurons[layers[layer_i][node_i]],input_size)
            nodes_label[node] = node
            G.add_node(node)
            voffset = layer_size / 2.0 - node_i
            hoffset = layer_i
            #For hidden layers shift down
            if not is_input and not is_output:
                voffset -= 6
                tmpcount = count*0.25 + np.random.random()
                if count%2 == 0:
                    tmpcount *= -1
                hoffset += tmpcount
            nodes_pos[node] = (hoffset, voffset)  #x,y position of node, find center point vertically, and offset from there

    edge_labels=dict([((u,v,),"%0.2f"%d['weight'])
                     for u,v,d in G.edges(data=True)])
    nodes_pos2 = nx.spring_layout(G)
    #import json
    #print(nodes_pos)
    ax = plt.figure(figsize=(10, 6)).add_subplot(1, 1, 1)
    #nx.draw_networkx_edge_labels(G,nodes_pos,edge_labels=edge_labels,ax=ax)
    nx.draw_networkx_edges(G, pos=nodes_pos, alpha=0.7, ax=ax)
    layerid = 0
    for layer in layers_name:
        if layerid == len(layers_name)-1:
            cc = '#FF6666'
        else:
            cc = '#66FFFF'
        nx.draw_networkx_nodes(G, nodelist=layer,
                           pos=nodes_pos, ax=ax,
                           node_color=cc, node_size=700)
        layerid+=1
    nx.draw_networkx_labels(G, labels=nodes_label,
                            pos=nodes_pos, font_size=14, ax=ax)
    ax.axis('off')
    pylab.show()
