#!/bin/sh

num=`ps -u | grep python |wc -l`
while [ "$num" -gt 1 ]
do
  date_info=`date`
  echo "$date_info::Still processing"
  sleep 60 #wait
  num=`ps -u | grep python |wc -l`
done
date_info=`date`
echo "$date_info::Completed"
python demoneat.py --grid 8 --grid_shutdown
