#sudo /usr/local/bin/anaconda3/bin/pip install google-api-python-client
#Download Linux Google Cloud SDK from https://cloud.google.com/sdk/docs/quickstart-linux
#tar xvfz ___
#__/install.sh
#gcloud init
#sudo /usr/local/bin/anaconda3/bin/pip install requests
#gcloud compute instances list
#gcloud compute instances start instance-2 instance-1 main
#gcloud compute instances stop instance-2 instance-1 main
#gcloud compute ssh dacboy@instance-2 --command="ls -l"
#gcloud compute copy-files ./${3}net.dump ./${3}net.dump.depth dacboy@instance-$1:~/wumpus_learn/$3
#gcloud compute ssh dacboy@instance-$1 --command="cd wumpus_learn; python demoneat.py --load_net $2"
import argparse
import googleapiclient.discovery

def create_Instance(compute,project, bucket, zone, instance_name, wait=True):
    print('Creating instance.')

    operation = create_instance(compute, project, zone, instance_name, bucket)
    wait_for_operation(compute, project, zone, operation['name'])


    print("""
Instance created.
It will take a minute or two for the instance to complete work.
Check this URL: http://storage.googleapis.com/{}/output.png
Once the image is uploaded press enter to delete the instance.
""".format(bucket))

    if wait:
        input()

    print('Deleting instance.')

    operation = delete_instance(compute, project, zone, instance_name)
    wait_for_operation(compute, project, zone, operation['name'])


def list_instances(compute, project, zone):
    result = compute.instances().list(project=project, zone=zone).execute()
    return result['items']

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--project_id', default="abstract-flame-165123", help='Your Google Cloud project ID.')
    parser.add_argument(
        '--bucket_name', default="", help='Your Google Cloud Storage bucket name.')
    parser.add_argument(
        '--zone',
        default='us-central1-c',
        help='Compute Engine zone to deploy to.')
    parser.add_argument(
        '--name', default='demo-instance', help='New instance name.')

    args = parser.parse_args()

    compute = googleapiclient.discovery.build('compute', 'v1')
    #create_instance(compute, args.project_id, args.bucket_name, args.zone, args.name)
    instances = list_instances(compute,args.project_id,args.zone)

    print('Instances in project %s and zone %s:' % (project, zone))
    for instance in instances:
        print(' - ' + instance['name'])
