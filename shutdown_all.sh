#!/bin/sh
max=100
for i in `seq 11 $max`
do
  gcloud compute --project "abstract-flame-165123" disks create "big-$i" --size "10" --zone "us-central1-c" --source-snapshot "snapshot-3" --type "pd-standard"
  gcloud compute --project "abstract-flame-165123" instances create "big-$i" --zone "us-central1-c" --machine-type "n1-standard-1" --subnet "default" --no-restart-on-failure --maintenance-policy "TERMINATE" --service-account "1079023533322-compute@developer.gserviceaccount.com" --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring.write","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --disk "name=big-$i,device-name=big-$i,mode=rw,boot=yes,auto-delete=yes"
  gcloud compute instances stop big-$i
  echo "Sleeping 5 seconds..."
  sleep 5
done
