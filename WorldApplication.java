/*
 * Wumpus-Lite, version 0.18 alpha
 * A lightweight Java-based Wumpus World Simulator
 *
 * Written by James P. Biagioni (jbiagi1@uic.edu)
 * for CS511 Artificial Intelligence II
 * at The University of Illinois at Chicago
 *
 * Thanks to everyone who provided feedback and
 * suggestions for improving this application,
 * especially the students from Professor
 * Gmytrasiewicz's Spring 2007 CS511 class.
 *
 * Last modified 3/5/07
 *
 * DISCLAIMER:
 * Elements of this application were borrowed from
 * the client-server implementation of the Wumpus
 * World Simulator written by Kruti Mehta at
 * The University of Texas at Arlington.
 *
 */


import java.io.BufferedWriter;
import java.io.BufferedReader;
//import java.io.Process;
//import java.io.ProcessBuilder;
import java.io.FileWriter;
import java.util.Random;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.util.Scanner;

class WorldApplication {

	private static String VERSION = "v0.18a";
	public static String CONST_EPISODE_DEFAULT = "default";
  //Train without wumpus and pits (just gold)
	public static String CONST_EPISODE_JUST_GOLD = "just_gold";
	//Train with only Wumpus where killing Wumpus is 1000 and dying is -1000
	public static String CONST_EPISODE_ONLY_WUMPUS = "only_wumpus";
	//Train with wumpus + pits and score 100 per every new spot achieved (no shooting allowed)
	public static String CONST_EPISODE_NO_GOLD = "no_gold";

	public static void main (String args[]) {

		int worldSize = 4;
		int numTrials = 1;
		int maxSteps = 50;

		boolean nonDeterministicMode = true;
		boolean randomAgentLoc = true;
		boolean userDefinedSeed = false;

		String outFilename = "wumpus_out.txt";
		String neuralnetworkfile = "nnfile.dump";
		String episodes = CONST_EPISODE_DEFAULT;

	    Random rand = new Random();
	    int seed = rand.nextInt();


		// iterate through command-line parameters
	    for (int i = 0; i < args.length; i++) {

	    	String arg = args[i];

	    	// if the world dimension is specified
	    	if (arg.equals("-d") == true) {

	    		if (Integer.parseInt(args[i+1]) > 1) {

	    			worldSize = Integer.parseInt(args[i+1]);
	    		}

	    		i++;
	    	}
	    	// if the maximum number of steps is specified
	    	else if (arg.equals("-s") == true) {

	    		maxSteps = Integer.parseInt(args[i+1]);
	    		i++;

	    	}
	    	// if the number of trials is specified
	    	else if (arg.equals("-t") == true) {

	    		numTrials = Integer.parseInt(args[i+1]);
	    		i++;

	    	}
	    	// if the random agent location value is specified
	    	else if (arg.equals("-a") == true) {

	    		randomAgentLoc = Boolean.parseBoolean(args[i+1]);
	    		i++;

	    	}
	    	// if the random number seed is specified
	    	else if (arg.equals("-r") == true) {

	    		seed = Integer.parseInt(args[i+1]);
	    		userDefinedSeed = true;
	    		i++;

	    	}
	    	// if the output filename is specified
	    	else if (arg.equals("-f") == true) {

	    		outFilename = String.valueOf(args[i+1]);
	    		i++;

	    	}
	    	// if the non-determinism is specified
	    	else if (arg.equals("-n") == true) {

	    		nonDeterministicMode = Boolean.parseBoolean(args[i+1]);
	    		i++;

	    	}
				// if the output filename is specified
	    	else if (arg.equals("-g") == true) {

	    		neuralnetworkfile = String.valueOf(args[i+1]);
	    		i++;

	    	}
				// type of episodes
	    	else if (arg.equals("-e") == true) {

	    		String tmpepisode = String.valueOf(args[i+1]);
					if(tmpepisode.equals(CONST_EPISODE_DEFAULT) || tmpepisode.equals(CONST_EPISODE_NO_GOLD) || tmpepisode.equals(CONST_EPISODE_JUST_GOLD)
					   || tmpepisode.equals(CONST_EPISODE_ONLY_WUMPUS))
				  {
						episodes = tmpepisode;
						//System.out.println("Running episode " + episodes);
					} else {
						//System.out.println("Episode " + tmpepisode + " does not exist, using default");
					}
	    		i++;

	    	}

	    }

	    try {

		    BufferedWriter outputWriter = null;//new BufferedWriter(new FileWriter(outFilename));

			//System.out.println("Wumpus-Lite " + VERSION + "\n");
			//outputWriter.write("Wumpus-Lite " + VERSION + "\n\n");

			//System.out.println("Dimensions: " + worldSize + "x" + worldSize);
		    //outputWriter.write("Dimensions: " + worldSize + "x" + worldSize + "\n");

		    //System.out.println("Maximum number of steps: " + maxSteps);
		    //outputWriter.write("Maximum number of steps: " + maxSteps + "\n");

		    //System.out.println("Number of trials: " + numTrials);
		    //outputWriter.write("Number of trials: " + numTrials + "\n");

		    //System.out.println("Random Agent Location: " + randomAgentLoc);
		    //outputWriter.write("Random Agent Location: " + randomAgentLoc + "\n");

			//System.out.println("Random number seed: " + seed);
			//outputWriter.write("Random number seed: " + seed + "\n");

		    //System.out.println("Output filename: " + outFilename);
		    //outputWriter.write("Output filename: " + outFilename + "\n");

		    //System.out.println("Non-Deterministic Behavior: " + nonDeterministicMode + "\n");
		    //outputWriter.write("Non-Deterministic Behavior: " + nonDeterministicMode + "\n\n");
				InputStream is = System.in;
				Scanner br = new Scanner(System.in);
				//InputStreamReader isr = new InputStreamReader(is);
				//BufferedReader br = new BufferedReader(isr);
				//String line;
				OutputStream os = System.out;
				OutputStreamWriter osw = new OutputStreamWriter(os);
				BufferedWriter bw = new BufferedWriter(osw);

				//System.out.printf("Output of running %s is:", Arrays.toString(args));

				//while ((line = br.readLine()) != null) {
				//  System.out.println(line);
				//}

		    char[][][] wumpusWorld = generateRandomWumpusWorld(seed, worldSize, randomAgentLoc, episodes);
		    Environment wumpusEnvironment = new Environment(worldSize, wumpusWorld, outputWriter);

		    int trialScores[] = new int[numTrials];
		    int totalScore = 0;

		    for (int currTrial = 0; currTrial < numTrials; currTrial++) {

		    	Simulation trial = new Simulation(wumpusEnvironment, maxSteps, outputWriter, nonDeterministicMode, br, bw, episodes);
		    	trialScores[currTrial] = trial.getScore();
					try{
						bw.write("iscore:"+((double)trialScores[currTrial])+"\n");
						bw.flush();
					}catch(Exception e){}

		    	//System.out.println("\n\n___________________________________________\n");
		    	//outputWriter.write("\n\n___________________________________________\n\n");

			    if (userDefinedSeed == true) {
			    	wumpusWorld = generateRandomWumpusWorld(++seed, worldSize, randomAgentLoc, episodes);
			    }
			    else {
			    	wumpusWorld = generateRandomWumpusWorld(rand.nextInt(), worldSize, randomAgentLoc, episodes);
			    }

			    wumpusEnvironment = new Environment(worldSize, wumpusWorld, outputWriter);

		    	System.runFinalization();
		    }

		    for (int i = 0; i < numTrials; i++) {

		    	//System.out.println("Trial " + (i+1) + " score: " + trialScores[i]);
		    	//outputWriter.write("Trial " + (i+1) + " score: " + trialScores[i] + "\n");
		    	totalScore += trialScores[i];

		    }

		    //System.out.println("\nTotal Score: " + totalScore);
		    //outputWriter.write("\nTotal Score: " + totalScore + "\n");

		    //System.out.println("Average Score: " + ((double)totalScore/(double)numTrials));
				try{
					bw.write("score:"+((double)totalScore/(double)numTrials)+"\n");
					bw.flush();
				}catch(Exception e){}
		    //outputWriter.write("Average Score: " + ((double)totalScore/(double)numTrials) + "\n");

		    //outputWriter.close();

	    }
	    catch (Exception e) {
	    	//System.out.println("An exception was thrown: " + e);
	    }

	    //System.out.println("\nFinished.");
	}

	public static char[][][] generateRandomWumpusWorld(int seed, int size, boolean randomlyPlaceAgent, String episodes) {

		char[][][] newWorld = new char[size][size][4];
		boolean[][] occupied = new boolean[size][size];

		int x, y;

		Random randGen = new Random(seed);

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				for (int k = 0; k < 4; k++) {
					newWorld[i][j][k] = ' ';
				}
			}
		}

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				occupied[i][j] = false;
			}
		}

		int pits = 2;

		// default agent location
		// and orientation
		int agentXLoc = 0;
		int agentYLoc = 0;
		char agentIcon = '>';

		// randomly generate agent
		// location and orientation
		if (randomlyPlaceAgent == true) {

			agentXLoc = randGen.nextInt(size);
			agentYLoc = randGen.nextInt(size);

			switch (randGen.nextInt(4)) {

				case 0: agentIcon = 'A'; break;
				case 1: agentIcon = '>'; break;
				case 2: agentIcon = 'V'; break;
				case 3: agentIcon = '<'; break;
			}
		}

		// place agent in the world
		newWorld[agentXLoc][agentYLoc][3] = agentIcon;

		// Pit generation
		if(episodes.equals(CONST_EPISODE_JUST_GOLD) || episodes.equals(CONST_EPISODE_ONLY_WUMPUS))
		{
			//skip
			//System.out.println("Skipped pit");
		} else {
			for (int i = 0; i < pits; i++) {

				x = randGen.nextInt(size);
				y = randGen.nextInt(size);

				while ((x == agentXLoc && y == agentYLoc) | occupied[x][y] == true) {
					x = randGen.nextInt(size);
					y = randGen.nextInt(size);
				}

				occupied[x][y] = true;

				newWorld[x][y][0] = 'P';

			}
		}

		// Wumpus Generation
		if(episodes.equals(CONST_EPISODE_JUST_GOLD))
		{
			//skip
			//System.out.println("Skipped Wumpus");
		} else {
			x = randGen.nextInt(size);
			y = randGen.nextInt(size);

			while (x == agentXLoc && y == agentYLoc) {
				x = randGen.nextInt(size);
				y = randGen.nextInt(size);
			}
			occupied[x][y] = true;

			newWorld[x][y][1] = 'W';
		}


		// Gold Generation
		if(episodes.equals(CONST_EPISODE_ONLY_WUMPUS) || episodes.equals(CONST_EPISODE_NO_GOLD))
		{
			//skip
			//System.out.println("Skipped Gold");
		} else {
			x = randGen.nextInt(size);
			y = randGen.nextInt(size);

			//while (x == 0 && y == 0) {
			//	x = randGen.nextInt(size);
			//	y = randGen.nextInt(size);
			//}

			occupied[x][y] = true;

			newWorld[x][y][2] = 'G';
		}

		return newWorld;
	}

}
