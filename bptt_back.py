# Imports
#Set the logging of Tensorflow to (0) DEBUG [default], INFO, WARN, ERROR, FATAL (4)
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from keras.models import Sequential,load_model
from keras.layers import Dense, Activation, Merge
from keras.layers.wrappers import TimeDistributed
from keras.layers.recurrent import LSTM
from keras.utils.np_utils import to_categorical
from keras.engine.topology import Layer
from keras import backend as K
import numpy as np

LEARN_LAST_NUM = 10 #last number of moves to learn from
CONST_NUM_ACTIONS = 6 #go forward,left, right, grab, shoot, no op
CONST_NUM_OBSERVATIONS = 5 #bump,glitter,breeze,stench,scream
CONST_OFFSET = 0.5 # Offset of the good or bad decision
CONST_NUM_EPOCH = 50
CONST_DISCOUNT_FACTOR = 0.5 #for crediting bad/good moves from the last move back to the first move

'''
Perform a distribution with weights to allow for automatic update of one weight chain.
'''
class MyLayer(Layer):
    def __init_(self, **kwargs):
        super(MyLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        #create a trainable weight variable for this layers
        self.output_dim = input_shape[1]
        self.W = self.add_weight(shape=(input_shape[1], self.output_dim),
                                 initializer='zero',
                                 trainable=False)
        super(MyLayer,self).build(input_shape)

    def call(self, x, mask=None):
        #x is same shape as input_shape_1
        #pick a random one base on x distribution
        #output action 0 to input_shape_1
        ###ISSUE WORKING HERE: K.get_value(x) doesn't get the Numpy array of the value, it crashes.
        action = np.random.choice(np.arange(self.output_dim),p=K.get_value(x)) #randomly choose based on the distribution
        action_array = to_categorical([action], CONST_NUM_ACTIONS)
        self.set_weights(action_array)
        return K.dot(x,self.W)

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], self.output_dim)

class mymodel:
    def __init__(self,filename=None):
        '''
        If filename is not None then load the keras model.  Else make a new model.
        '''
        if filename == None:
            # Model
            models = []
            h_layer_1_size = 100
            h_layer_2_size = 100 #this is LSTM
            #Make
            layer1 = LSTM(output_dim=h_layer_1_size, input_shape=(1,CONST_NUM_OBSERVATIONS), activation='relu')
            for ii in range(CONST_NUM_ACTIONS):
                model = Sequential() #one per output
                model.add(layer1) #initial number is output_dim, stateful means it has memory of the last time called (using this because not batching the input)
                model.add(Dense(output_dim=1)) #only one action
                model.compile(loss="mean_absolute_error",optimizer="sgd",metrics=['accuracy'])
                models.append(model)
            end_model = Sequential()
            end_model.add(Merge(models, mode='concat'))
            #end_model.add(Activation("softmax"))
            end_model.compile(loss="categorical_crossentropy",optimizer="sgd",metrics=['accuracy'])

            #model.add(Dense(output_dim=h_layer_1_size, input_shape=(1,CONST_NUM_OBSERVATIONS))) #TimeDistributed allows a time aspect to the input, making the input be 3D
            #model.add(Activation("relu"))
            #model.add(LSTM(output_dim=h_layer_2_size, stateful=True)) #initial number is output_dim, stateful means it has memory of the last time called (using this because not batching the input)
            #model.add(Dense(output_dim=h_layer_2_size)) #TimeDistributed allows a time aspect to the input, making the input be 3D
            #model.add(Activation("softmax")) #makes each output be 0-1 and all outputs to add up to 1
            #model.add(MyLayer()) #makes each output be 0-1 and all outputs to add up to 1
            #model.compile(loss="categorical_crossentropy",optimizer="sgd",metrics=['accuracy','categorical_crossentropy'])
            #kullback_leibler_divergence is used to compare probability distributions Q to P
            self.models = models
            self.end_model = end_model
        else:
            pass #doesn't work with multiple models
            #self.model = load_model(filename)

        self.distribution = None
        self._all_learn = []
        self.reset()

    def reset(self):
        self._actions = []
        self._observations = []
        self._actions_dist = []
        self._repredict = []
        self._actions_num = []
        self._actions_single = []
        self._actions_scale = []
        self._x_test = []
        #self.model.reset_states() #reset LSTM state

    '''
    Predict the next action, given the new observation.
    It will concat that observation onto the end of an array of all the observations across time up till now.
    Return the action to take, after performing a random action based on the distribution across actions the network gives.
    To get the distribution used, see self.distribution.
    '''
    def predict(self,obs):
        assert(len(obs) == CONST_NUM_OBSERVATIONS) #verify correct size of observations
        self._observations.append(np.array([obs]))
        X_test = np.array(self._observations)
        model = self.end_model
        proba = model.predict_proba(X_test)
        #perform a softmax 0-1 with sum to 1
        proba_shift = proba[-1] - np.min(proba[-1])
        proba_scale = np.sum(proba_shift)
        action_dist = proba_shift/proba_scale
        action = np.random.choice(np.arange(CONST_NUM_ACTIONS),p=action_dist) #randomly choose based on the distribution
        #Save the array form of the action chosen too (e.g. [0,0,1,0,0,0])
        action_array = to_categorical([action], CONST_NUM_ACTIONS)

        #Save for reference
        self.distribution = action_dist #temporary distribution for last action

        #Save for learning
        self._x_test.append(X_test)
        self._repredict.append(proba[-1]) #save for analysis of change
        self._actions_dist.append(proba[-1]) #save distribution for all time steps
        self._actions_scale.append(proba_scale)
        self._actions_single.append(proba[-1][action])
        self._actions.append(action_array[-1])
        self._actions_num.append(action)
        return action


    '''
    Uses the observations from a single game and fits the data.
    Adds the current scenario information to the training package
    '''
    def add_to_learning(self,score):
        #score range should -1100 to 1000; offset to -1050 to 1050, then scale to -1 to 1 by dividing by 1050
        #new_score = (score*1.0 + 50.0)/1150.0
        #The only reason a positive score happens is if the Gold is picked up.
        #  Otherwise between -100 and 0 is avoiding death.  Anything less is bad.  So maybe -20 is middle.  So offset
        new_score = (score*1.0 + 50.0)/1150.0
        X_train = self._x_test
        #Modify the expected to be higher or lower for the decision chosen based on the score.
        #COULD add decrementing 0.99 each timestep from end
        #COULD add
        Y_train_1 = self._actions_num
        Y_train_2 = np.array(self._actions_single) + np.array(self._actions_scale) * CONST_OFFSET * new_score # need to scale new_score based on single

        self._all_learn.append( (X_train,Y_train_1, Y_train_2) )


    '''
    Learn from all the scenarios saved.
    '''
    def learn(self):
        #only last 10 moves before end
        #for (X_train,Y_train) in self._all_learn:
        for (X_train, Y_train_1, Y_train_2) in self._all_learn:
            num_updates = min(LEARN_LAST_NUM,len(X_train))
            cur_dis_fact = 1.0 #start with full
            for i in range(num_updates):#max of 10 or length of steps
                ind = -i-1 #start from end, so i=0,1,2,3,4 --> ind=-1,-2,-3,-4,-5
                self.models[Y_train_1[ind]].fit([ X_train[ind] ], [ Y_train_2[ind]*cur_dis_fact ], nb_epoch=CONST_NUM_EPOCH)
                cur_dis_fact *= CONST_DISCOUNT_FACTOR #make it progressively smaller changes back in time
        self._all_learn = [] #delete what we trained


    def save(self,filename):
        pass #doesn't work with multiple models
        #self.model.save(filename)

    '''
    Compare previous prediction before learning with new prediction after learning.
    self._observations is the X_test and the Y_test is the self._repredict
        #Setup table:
        # bump      0   1   0   1   ...
        # glitter   0   0   1   1   ...
        # ...
        # ACTION    FORWARD
    '''
    def learn_analysis(self):
        cur_num = -1
        for Y_test in self._repredict:
            cur_num += 1
            proba = self.model.predict_proba(np.array(self._observations[:cur_num+1])) #needs +1 because includes -1
            Y_test_new = proba[-1]
            chg = Y_test_new - Y_test
            print(self._observations[cur_num],self._actions[cur_num],chg)

    '''
    Prints information about the neural network probablity distribution given a
    '''
    def net_info(self):
        import sys
        import os

        def getname_percept(idx):
            if idx == 0:
                return "bump"
            elif idx == 1:
                return "glitter"
            elif idx == 2:
                return "breeze"
            elif idx == 3:
                return "stench"
            elif idx == 4:
                return "scream"
        def getname_action(idx):
            if idx == 0:
                return "FOWARD"
            elif idx == 1:
                return "RIGHT"
            elif idx == 2:
                return "LEFT"
            elif idx == 3:
                return "GRAB"
            elif idx == 4:
                return "SHOOT"
            elif idx == 5:
                return "NOOP"

        #cycle through all combinations
        #make tabulate
        from tabulate import tabulate
        num_neurons = 5
        num_actions = 1
        last_row = num_neurons
        #Setup table:
        # bump      0   1   0   1   ...
        # glitter   0   0   1   1   ...
        # ...
        # ACTION    FORWARD
        multi_action = True #If only one action, or random action
        if multi_action:
            num_actions = 6
        tbl = [ [ 0 for _ in range(2**num_neurons+1) ] for _ in range(num_neurons+num_actions)]#  [[0.0]*(len(neurons)+1)]*(len(neurons))
        for ii in range(num_neurons):
            tbl[ii][0] = getname_percept(ii)
        if multi_action:
            for ii in range(num_actions):
                tbl[last_row+ii][0] = getname_action(ii)
        else:
            tbl[last_row][0] = "ACTION"

        for idx_col in range(2**num_neurons):
            inpercepts = idx_col
            true_idx_col = idx_col + 1
            nnpercept = [] #bump,glitter,breeze,stench,scream
            for ii in range(5):
                val = ( inpercepts & (2**ii) ) / (2**ii)
                nnpercept.append( val )
                tbl[ii][true_idx_col] = int(val)
            nnpercept = np.array(nnpercept)
            self.reset() #clean out previous observations
            action = self.predict(nnpercept)
            if multi_action:
                waction = self.distribution
                for ii in range(num_actions):
                    tbl[last_row+ii][true_idx_col] = ('%0.3f' % (waction[ii]))
            else:
                tbl[last_row][true_idx_col] = getname_action(action)

        #print("Weights for connection from Neuron on Row --> Neuron on Column\n")
        return tabulate(tbl)


'''
Current status:
LSTM doesn't seem to be working, always get the same prediction for the same observation.  But this might be because it hasn't learned to remember (check init of LSTM).
Train more and see what happens.
'''

#To train
#model.fit(X_train, Y_train, nb_epoch=5, batch_size=32)

#pred = model.predict(x) #gets (class, description, probablility)
#classes = model.predict_classes(X_test, batch_size=32)
#proba = model.predict_proba(X_test)
#See https://keras.io/layers/recurrent/


'''
So the idea:
Do policy roll-outs of 200 games with the network, using predict_proba with
    X_test = [
                 [ 0 0 1 0 0], [ 0, 1 ,0, 1, 1 ]
             ]
    making it 2D but with only a sequece of the current game observations added into one element, because we only have one game
Then take the game score and use some equation to determine if it was a good or bad game (in proportions).
For each game, use the sequence of probabilities and multiply the highest by a proportion based on if good or bad (i.e. increase if good, decrease if bad) and rescale with softmax and make that the Y_label.
Then do a fit with the X_train = [ [[0,0,1,0,0], [0,1,0,0,0]],[0,1,1,0,0],[0,1,1,0,0]]  3D because all games will be in there.

Still trying to figure out the TimeDistributed() if it is needed.  Do not feel the LSTM(stateful=True) is needed because I am sending the entire sequece each time, so no memory needed.
The loss should probably be log loss??
'''
