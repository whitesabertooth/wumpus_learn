'''
Created on Sep 8, 2016

@author: whitesabertooth
'''
#################################
#  I M P O R T S
#################################
import numpy as np
from neatsetup import MyNEAT,NEAT_types
from neatsetup import GenomeRun
import sys
import os



def loadNet(filename):
    sys.stdout.write("Loading neural network from %s..." %(filename))
    sys.stdout.flush()
    net = GenomeRun(filename=filename)
    #with open(filename+'.depth','r') as f:
    #    net.depth = int(f.read())
    net.depth = 8 #hard coded
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net

def getname_percept(idx):
    if idx == 0:
        return "bump"
    elif idx == 1:
        return "glitter"
    elif idx == 2:
        return "breeze"
    elif idx == 3:
        return "stench"
    elif idx == 4:
        return "scream"
def getname_action(idx):
    if idx == 0:
        return "FOWARD"
    elif idx == 1:
        return "RIGHT"
    elif idx == 2:
        return "LEFT"
    elif idx == 3:
        return "GRAB"
    elif idx == 4:
        return "SHOOT"
    elif idx == 5:
        return "NOOP"

def printNetInfo(filename):
    mygenome = loadNet(filename)
    #cycle through all combinations
    #print("in: ",nnpercept,", out: %d"%(action))
    #make tabulate
    from tabulate import tabulate
    num_neurons = 5
    num_actions = 1
    last_row = num_neurons
    #Setup table:
    # bump      0   1   0   1   ...
    # glitter   0   0   1   1   ...
    # ...
    # ACTION    FORWARD
    multi_action = True #If only one action, or random action
    if multi_action:
        num_actions = 6
    tbl = [ [ 0 for _ in range(2**num_neurons+1) ] for _ in range(num_neurons+num_actions)]#  [[0.0]*(len(neurons)+1)]*(len(neurons))
    for ii in range(num_neurons):
        tbl[ii][0] = getname_percept(ii)
    if multi_action:
        for ii in range(num_actions):
            tbl[last_row+ii][0] = getname_action(ii)
    else:
        tbl[last_row][0] = "ACTION"

    for idx_col in range(2**num_neurons):
        inpercepts = idx_col
        true_idx_col = idx_col + 1
        nnpercept = [] #bump,glitter,breeze,stench,scream
        for ii in range(5):
            val = ( inpercepts & (2**ii) ) / (2**ii)
            nnpercept.append( val )
            tbl[ii][true_idx_col] = int(val)
        nnpercept = np.array(nnpercept)
        action = mygenome.testState(nnpercept)
        if multi_action:
            waction = mygenome.getlast_weights()
            for ii in range(num_actions):
                tbl[last_row+ii][true_idx_col] = ('%0.3f' % (waction[ii]))
        else:
            tbl[last_row][true_idx_col] = getname_action(action)

    #print("Weights for connection from Neuron on Row --> Neuron on Column\n")
    print(tabulate(tbl))


if __name__ == '__main__':
    import argparse
    #=======================
    # Process arguments
    parser = argparse.ArgumentParser(description='Demo NEAT.')
    parser.add_argument('net_file', metavar='FILE',
                        help='File of a dumped neural network')
    args = parser.parse_args()

    printNetInfo(args.net_file)
