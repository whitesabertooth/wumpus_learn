'''
Created on Sep 23, 2016

@author: whitesabertooth

This is a test to prove that my tool reproduces ViZDoom scenarios the same each time.
'''

import vizdoom
import vizdoomsetup
import numpy as np
resolution = (30,45)
def setupGame():
    game = vizdoomsetup.VIZDOOM({})
    #game.load_config('config/simpler_basic.cfg')
    game.startgame(seed=55)
    return game.game

def rungame():
    '''Save first image of multiple episodes
    '''
    game = setupGame()
    saveimgs = []
    num_test_episodes = 10
    #while not game.is_episode_finished():
    for test in range(num_test_episodes):
        game.new_episode()
        img = game.get_state().image_buffer
        newimg = vizdoomsetup.preprocess(img, resolution)
        saveimgs.append(newimg)
        #game.advance_action()
    game.close()
    return saveimgs


if __name__ == '__main__':
    img1 = rungame()
    img2 = rungame()
    #compare screens between executions
    for idx in range(len(img1)):
        if len(img2) <= idx:
            print("Size diff img2 %d <= img1 %d" %(len(img2),idx))
            exit(1)
        for xx in range(len(img1[idx])):
            if img1[idx][xx] != img2[idx][xx]:
                print("Diff on img %d at %d"%(idx,xx))
                
    pass