'''
Created on Sep 8, 2016

@author: whitesabertooth
'''
#################################
#  I M P O R T S
#################################
import numpy as np
import pickle
import time
from neatsetup import MyNEAT,NEAT_types
import sys
from neatsetup import GenomeRun
import os
import subprocess
import re

## GLOBAL CONST
'''ADD Progress bar
def update_progress(progress):
    print '\r[{0}] {1}%'.format('#'*(progress/10), progress)
looks like this: [ ########## ] 100%
'''
score_offset = 300 #worst possible score...this is a guess; added to score to make base of 0
save_best_genome_file = 'gen.dump'
save_best_net_file = 'net.dump'
save_best_pop_file = 'pop.dump'
all_episodes = ['default','just_gold','only_wumpus','no_gold']

neat_params = {}
'''
Times running doom =
    num_generations * num_training * population_size
'''
neat_params['num_generations'] = 20 #Number of epochs to train a population group
neat_params['num_trainings'] = 1 #Number of population groups to train
neat_params['population_size'] = 100 #Size of population
neat_params['num_inputs'] = 5
neat_params['num_outputs'] = 6 #go forward,left, right, grab, shoot, no op
#neat_params['resolution'] = resolution  #for HyperNEAT need the x,y resolution
neat_params['record_every'] = 5 #how often to record playbacks and pop by generations
neat_params['memory_inputs'] = 0 #How many previous inputs/action to feed the network
neat_params['num_hidden'] = 0 #How many hidden nodes in the hidden layer
neat_params['num_scenarios'] = 1000 #How many scenarios - TODO: hard coded for REMOTE
neat_params['override_pop'] = False #If True will override a loaded population with the Parameters

def evaluate(mygenome,seed,dolearn,record,num_scenarios,prefix,episode="default"):
    fitness = 0
    fitness_all = []
    NUM_RUNS = 1 #get average across these runs for the fitness - because of Non-deterministic actions
    for run_idx in range(NUM_RUNS):
        mygenome.reset() #forget previous inputs
        proc = subprocess.Popen(['./run.sh','%d'%(num_scenarios),'%d'%(seed),episode],stdin=subprocess.PIPE,stdout=subprocess.PIPE,universal_newlines=True)#universal_newlines=True makes the PIPEs str instead of byte
        notend = True
        while notend:
            line = proc.stdout.readline()
            #print(line)
            if line[0] == 's':#final average score
                notend = False
                fitness_all.append(float(line.split(":")[1]))
            elif line[0] == 'i':#print intermediate score
                #print(line)
                pass
            else:
                inpercepts = int(line)
                #split the percepts by 2^(0-4)
                nnpercept = [] #bump,glitter,breeze,stench,scream
                for ii in range(5):
                    nnpercept.append( ( inpercepts & (2**ii) ) / (2**ii) )
                nnpercept = np.array(nnpercept)
                action = mygenome.testState(nnpercept)
                #print("in: ",nnpercept,", out: %d"%(action))
                proc.stdin.write(str(action)+"\n")
                if proc.returncode == None:
                    proc.stdin.flush()
                else:
                    print("terminated too early")
                    notend = False
        proc.terminate() #should be done anyway, but just to make sure
    #Calculate the average of all the runs
    if len(fitness_all) > 0:
        fitness = np.mean(np.array(fitness_all))
    sys.stdout.write(".")
    sys.stdout.flush()
    return fitness

#For each splice job index into this array to find the instance-#
jenkins_info = {'PRE-01':('pre-1',"us-central1-c")}
grid_servers = [
('big-1',"us-central1-c"),
('big-2',"us-central1-c"),
('big-3',"us-central1-c"),
('big-4',"us-central1-c"),
('big-5',"us-central1-c"),
('big-6',"us-central1-c"),
('big-7',"us-central1-c"),
('big-8',"us-central1-c"),
('big-9',"us-east1-c"),
('big-10',"us-east1-c"),
('big-11',"us-east1-c"),
('big-12',"us-east1-c"),
('big-13',"us-east1-c"),
('big-14',"us-east1-c"),
('big-15',"us-east1-c"),
('big-16',"us-east1-c"),
('big-17',"us-west1-b"),
('big-18',"us-west1-b"),
('big-19',"us-west1-b"),
('big-20',"us-west1-b"),
('big-21',"us-west1-b"),
('big-22',"us-west1-b"),
('big-23',"us-west1-b"),
('big-24',"us-west1-b")
]

def remote_evaluate(mygenome,seed,dolearn,record,num_scenarios,prefix,episode="default"):
    fitness = 0.0
    #save genome
    if not dolearn:
        part_name = prefix+'-%d'%(record)
        full_name = part_name + 'net.dump'
        saveNet(mygenome,full_name,display=False)
        #offset = seed%neat_params['num_grid']
    #Mod the seed, which is the index of Genome list by the number of grids to run
    #proc = subprocess.Popen(['./run_gce.sh',grid_servers[offset][0],part_name,grid_servers[offset][1]],stdin=subprocess.PIPE,stdout=subprocess.PIPE,universal_newlines=True)#universal_newlines=True makes the PIPEs str instead of byte

    #notend = True
    #while notend:
    #    line = proc.stdout.readline()
    #    #print(line)
    #    if line[0] == '.':#final average score
    #        notend = False
    #        fitness = float(line.split(":")[1])
    #    else:
    #        proc.poll()
    #        if proc.returncode != None:
    #            print("terminated too early")
    #            notend = False
    #proc.terminate() #should be done anyway, but just to make sure

    #count = 1 # retries
    #timeout = 90 #seconds
    #while count > 0:
    #    count -=1
    #    try:
    #        output = subprocess.check_output(['./run_gce.sh',grid_servers[offset][0],part_name,grid_servers[offset][1]],universal_newlines=True,timeout=timeout)#stderr=subprocess.STDOUT
    #        found = False
    #        for line in output.split("\n"):
    #            if line and len(line) > 0 and line[0] == '.':#final average score
    #                fitness = float(line.split(":")[1])
    #                found = True
    #                count = 0
    #                break
    #        if not found:
    #            print("No result(%s:%d)"%(grid_servers[offset][0],count))
    #    except subprocess.TimeoutExpired:
    #        print("Timeout expired(%s:%d)"%(grid_servers[offset][0],count))
    #    except subprocess.CalledProcessError:
    #        print("Error processing(%s:%d)"%(grid_servers[offset][0],count))

    #    if count > 0:
    #        #wait one second before tyring again
    #        time.sleep(1)

    import jenkinsapi
    from jenkinsapi.jenkins import Jenkins
    bnum = None
    try:
        server = Jenkins('http://localhost:8080', username='admin', password='e72503af6a3fd5990f0521c4027630c5')
        tjob = server.get_job('Test')
        if not dolearn:
            #If suppose to submit to Jenkins
            data_file = open(full_name, 'rb')
            data_file2 = open(full_name+".depth", 'rb')
            running_job_object = tjob.invoke(securitytoken='blah',block=False,build_params={'seed':str(seed),'episode':episode},
                                            files={'ggnet.dump': data_file,'ggnet.dump.depth': data_file2})
            data_file.close()
            data_file2.close()

            #save the queue object
            bnum = running_job_object
        else:
            #If suppose to monitor Jenkins
            # then mygenome is the mapping of build number to fitness array element
            from requests import HTTPError
            from jenkinsapi.custom_exceptions import NotBuiltYet
            map_build_num = mygenome
            maxwait = 60*60*3 #(3 hrs) seconds for all executions per Generation
            sleeptime = 10 #seconds to wait between checking if running
            found_active = True #start true to get into the while
            cnt = 0
            bnum = [0.0] * seed #final fitness array initialize to 0.0; seed is the length of the genome_list
            post_job_all = {}
            while found_active: #cnt < maxwait and
                found_active = False #keep track of if there is an active job still
                all_keys = list(map_build_num.keys())
                for idx in all_keys:
                    build_queue = map_build_num[idx]
                    #Check if job has started
                    try:
                        if idx in post_job_all:
                            post_job = post_job_all[idx]
                            post_job.poll()
                        else:
                            build_queue.poll()
                            post_job = build_queue.get_build()
                            post_job_all[idx] = post_job

                        #Has started...
                        if post_job.is_running():
                            #Job still running
                            found_active = True
                            #TODO: Look for extra long running job to kill it
                        else:
                            #Job finished
                            #get env variables
                            del map_build_num[idx] #remove from list to look for
                            env = post_job.get_env_vars()
                            if 'Fitness' in env:
                                #save a corrresponding offset
                                bnum[idx] = float(env['Fitness'])
                            else:
                                #Search the console for the number if for some reason it crashed after printing fitness
                                console = post_job.get_console()
                                import re
                                match = re.search("Fitness=([\-0-9\.]+)",console)
                                if match:
                                    bnum[idx] = float(match.group(1))
                                else:
                                    print("Error finding fitness environment variable and console (%d)" %(post_job.get_number()))
                            sys.stdout.write(".")
                            sys.stdout.flush()
                    except (NotBuiltYet, HTTPError):
                        #Not started yet, so move to next one
                        found_active = True
                    except Exception as e:
                        #Not started yet, so move to next one
                        print(e)
                        found_active = True
                #--end For
                cnt += sleeptime
                time.sleep(sleeptime) #sleep a bit
            #--end while
            #Check if went past total wait time
            if cnt >= maxwait:
                print("Timeout of jenkins running")
    except Exception as e:
        print(e)
        print("Had exception with jenkins")

    if not dolearn:
        os.remove(full_name) #remove temp file
        os.remove(full_name+".depth") #remove temp file
    return bnum



def doTraining(initgenome=None,initpop=None):
    neat = None

    #Set parameters based on vizdoom
    print("Starting the training!")
    sub_eval = evaluate
    if neat_params['use_grid']:
        sub_eval = remote_evaluate
    neat = MyNEAT(neat_params,sub_eval,initgenome=initgenome,initpop=initpop)

    time_start = time.time()
    bestgen = neat.train()
    print("Total elapsed time: %.2f minutes" % ((time.time() - time_start) / 60.0))
    #Finished Learning
    return bestgen

def saveNet(net,filename=save_best_net_file,display=True):
    if display:
        sys.stdout.write("Saving the best neural network to %s..." %(filename))
        sys.stdout.flush()
    net.save(filename)
    with open(filename+'.depth','w') as f:
        f.write(str(net.depth))
    if display:
        sys.stdout.write("done\n")
        sys.stdout.flush()

def loadNet(filename=save_best_net_file,config=None):
    sys.stdout.write("Loading neural network from %s..." %(filename))
    sys.stdout.flush()
    net = GenomeRun(filename=filename,config=config)
    #with open(filename+'.depth','r') as f:
    #    net.depth = int(f.read())
    net.depth = 8 #hardcode because doesn't matter currently
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net

def saveGenome(net,filename=save_best_genome_file):
    sys.stdout.write("Saving the best genome to %s..." %(filename))
    sys.stdout.flush()
    net.Save(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()

def loadGenome(filename=save_best_genome_file):
    import MultiNEAT as NEAT
    sys.stdout.write("Loading genome from %s..." %(filename))
    sys.stdout.flush()
    net = NEAT.Genome(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net


def savePop(net,filename=save_best_pop_file):
    sys.stdout.write("Saving the best population to %s..." %(filename))
    sys.stdout.flush()
    net.Save(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()

def loadPop(filename=save_best_pop_file):
    import MultiNEAT as NEAT
    sys.stdout.write("Loading population from %s..." %(filename))
    sys.stdout.flush()
    net = NEAT.Population(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net

def merge(ss):
    area = {}
    for aa in ss:
        if aa[1] not in area:
            area[aa[1]] = []
        area[aa[1]].append(aa[0])
    return area

def startJenkins(instances):
    '''This has not been tested'''
    jenkins_node = "PRE-01"
    cmd = "java -jar ../jenkins-cli.jar -s http://localhost:8080/ connect-node %s --username admin --password a7895123" % (jenkins_node)
    timeout = 50 #seconds
    try:
        output = subprocess.check_output(cmd,universal_newlines=True,timeout=timeout)#stderr=subprocess.STDOUT
    except subprocess.TimeoutExpired:
        print("Node restart - Timeout expired(%s)"%(jenkins_node))
    except subprocess.CalledProcessError:
        print("Node restart - Error processing(%s)"%(jenkins_node))


def startInstances(instances):
    '''Start instances, when complete all will be started'''
    area = merge(instances)
    #print(area)
    err = False
    for bb in area:
        instances_str = " ".join(area[bb])
        print("Starting instances..." + instances_str)
        cmds = ['gcloud', 'compute', 'instances',"start","--zone",bb] + area[bb]
        proc = subprocess.Popen(cmds,stdin=subprocess.PIPE,stdout=subprocess.PIPE,universal_newlines=True)#universal_newlines=True makes the PIPEs str instead of byte
        notend = True
        while notend:
            line = proc.stdout.readline()
            #Check if done:
            proc.poll()
            if proc.returncode != None:
                notend = False
            else:
                #Verify everything updated
                if not line.startswith("Updated"):
                    print("  Error starting: " + line)
                    err = True
                #print(line)
        proc.terminate() #should be done anyway, but just to make sure
    return err

def stopInstances(instances):
    '''Stop instances, when complete all will be started'''
    area = merge(instances)
    err = False
    for bb in area:
        instances_str = " ".join(area[bb])
        print("Stopping instances..." + instances_str)
        cmds = ['gcloud', 'compute', 'instances',"stop","--zone",bb] + area[bb]
        proc = subprocess.Popen(cmds,stdin=subprocess.PIPE,stdout=subprocess.PIPE,universal_newlines=True)#universal_newlines=True makes the PIPEs str instead of byte
        notend = True
        while notend:
            line = proc.stdout.readline()
            #Check if done:
            proc.poll()
            if proc.returncode != None:
                notend = False
            else:
                #Verify everything updated
                if not line.startswith("Updated"):
                    print("  Error stopping: " + line)
                    err = True
                #print(line)
        proc.terminate() #should be done anyway, but just to make sure
    return err


if __name__ == '__main__':
    import argparse
    #=======================
    # Process arguments
    parser = argparse.ArgumentParser(description='Demo NEAT.')
    parser.add_argument('--load_genome', dest='genome', metavar='FILE',
                        help='File of a dumped genome')
    parser.add_argument('--test_net', dest='testnet', metavar='FILE',
                        help='File of a dumped neural network to test')
    parser.add_argument('--load_net', dest='net', metavar='FILE',
                        help='File of a dumped neural network')
    parser.add_argument('--gen', type=int, dest='num_gen', metavar='NUM',
                        help='Number of epochs/generations to train a population group.')
    parser.add_argument('--train', type=int, dest='num_train', default=1, metavar='NUM',
                        help='Number of population groups to train.')
    parser.add_argument('--pop', type=int, dest='num_pop', metavar='NUM',
                        help='Size of the population for a group.')
    parser.add_argument('--load_pop', dest='pop', metavar='FILE',
                        help='File of a dumped population')
    parser.add_argument('--prefix', dest='prefix', metavar='PREFIX',
                        help='Prefix to add to all dumped files')
    parser.add_argument('--type', dest='type', metavar='TYPE',
                        help='Type of NEAT to use (e.g. NEAT or ESHyperNEAT)')
    parser.add_argument('--episode', dest='episode', metavar='TYPE',
                        help='Type of episode to run for Wumpus World (default, only_wumpus, just_gold)')
    parser.add_argument('--learn', dest='learn', action='store_true', default=False,
                        help='Make it learn while executing')
    parser.add_argument('--last_gen', dest='lastgen', type=int, metavar='NUM',
                        help='Last generation executed')
    parser.add_argument('--grid', dest='grid', action='store_true', default=False,
                        help='Use the grid nodes')
    #parser.add_argument('--grid_shutdown', dest='grid_shutdown', action='store_true', default=False,
    #                    help='Shutdown all grid - only')
    #parser.add_argument('--grid_start', dest='grid_start', action='store_true', default=False,
    #                    help='Start all grid - only')
    parser.add_argument('--build_num', dest='build_num', type=int, metavar='NUM',
                        help='Build Number for Jenkins', default=0)
    parser.add_argument('--seed', dest='seed', type=int, metavar='NUM',
                        help='Seed for random games', default=0)
    parser.add_argument('--override_pop', dest='override_pop', action='store_true', default=False,
                        help='Override the population parameters if loaded')

    lastgen = 0
    prefix = "best"
    neattype = 'NEAT'
    episode = "default"
    use_grid = False
    args = parser.parse_args()

    if args.episode:
        if args.episode in all_episodes:
            episode = args.episode
            print("Performing episodes " + episode)
        else:
            print("Episode %s does not exist" % args.episode)
            exit(1)

    neat_params['episode_type'] = episode
    if args.override_pop:
        neat_params['override_pop'] = True
    if args.grid:
        use_grid = True
        neat_params['num_grid'] = 1#args.grid-hard coded
        #instances = grid_servers[:args.grid]

        #if just shutdown then do it
        #if args.grid_shutdown:
        #    stopInstances(instances)
        #    exit(0)

        #if args.grid_start:
        #    startInstances(instances)
        #    exit(0)

    neat_params['use_grid'] = use_grid

    if args.prefix:
        prefix = args.prefix

    if args.type:
        if args.type not in NEAT_types:
            print("--type is not in NEAT types")
            parser.print_help()
            exit(1)
        neattype = args.type

    neat_params['prefix'] = prefix
    neat_params['type'] = neattype
    neat_params['learn'] = False #args.learn - hard code because not used/doesn't work
    if args.learn:
            print("Using learning")

    #If test net set then just show demo
    if args.testnet:
        net = loadNet(args.testnet,neat_params)
        print("Testing neural network loaded")
        print(net.net_info())
        print(net.net_info_recur())
        exit()

    if args.net:
        net = loadNet(args.net,neat_params)
        print("Using neural network loaded to watch")

        fitness = evaluate(net,args.seed,False,False,neat_params['num_scenarios'],None,episode=episode)
        output = "Fitness=%.5f"%fitness
        print(output)
        #Used by Jenkins to set environment variables
        with open(str(args.build_num)+'/out.prop','w') as f:
            f.write(output)
        exit()

    if args.lastgen:
        lastgen = args.lastgen
    neat_params['lastgen'] = lastgen
    initgenome = None
    initpop = None
    if args.genome:
        initgenome = loadGenome(args.genome)
    if args.pop:
        initpop = loadPop(args.pop)
    if args.num_gen:
        neat_params['num_generations'] = args.num_gen #Number of epochs to train a population group
    if args.num_train:
        neat_params['num_trainings'] = args.num_train #Number of population groups to train
    if args.num_pop:
        neat_params['population_size'] = args.num_pop #Size of population


    #=======================
    # Do training
    best = doTraining(initgenome=initgenome,initpop=initpop)
    #=======================
    # Save data
    print("======================================")
    import os
    if os.path.exists(prefix+save_best_net_file):
        os.remove(prefix+save_best_net_file)
    saveNet(best['net'],prefix+save_best_net_file)
    if os.path.exists(prefix+save_best_genome_file):
        os.remove(prefix+save_best_genome_file)
    saveGenome(best['genome'], prefix+save_best_genome_file)
    if os.path.exists(prefix+save_best_pop_file):
        os.remove(prefix+save_best_pop_file)
    savePop(best['pop'],prefix+save_best_pop_file)
