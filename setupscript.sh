#!/usr/bin/sh
sudo apt-get install -y python3 python3-dev python3-pip python3-setuptools git python3-numpy htop
sudo apt-get install -y python python-dev python-pip python-setuptools python-numpy libboost-python-dev libboost-serialization-dev libboost-system-dev
sudo pip3 install snakeviz tabulate
sudo pip install snakeviz tabulate futures
sudo apt-get install -y default-jre
git clone https://dacboy@bitbucket.org/dacboy/wumpus_learn.git
mkdir mneat
cd mneat
tar xvfz ../wumpus_learn/mneat.tar.gz
#sudo pip install boost

#make a 2GB swap space for extra memory
sudo dd if=/dev/zero of=/swapfile bs=1024 count=2048k
sudo mkswap /swapfile
sudo chown root:root /swapfile
sudo chmod 0600 /swapfile
sudo swapon /swapfile
#echo 10 | sudo tee /proc/sys/vm/swappiness
#echo vm.swappiness = 10 | sudo tee -a /etc/sysctl.conf

sudo pip install .

