#!/bin/sh
# Copies the Network files and then runs the analysis and waits for the result to be printed to STDOUT
# run_gce.sh <instance> <net prefix>
#   net prefix: ./<net prefix>net.dump  (also ./<net prefix>net.dump.depth)
#   instance number: dacboy@<instance>
NAME="./${2}net.dump"
gcloud compute copy-files $NAME ./${2}net.dump.depth dacboy@$1:~/wumpus_learn --zone="$3"
#kill previous executions first - pkill -f demoneat; 
gcloud compute ssh dacboy@$1 --command="cd wumpus_learn; python demoneat.py --load_net $NAME; rm $NAME" --zone="$3"
