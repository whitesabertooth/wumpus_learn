-- Simple torch-rnn (https://github.com/Element-Research/rnn) demo
-- based on torch-rnn library demos
require 'rnn'
torch.manualSeed(0) -- set seed


rho = 5 --number of time steps for backprop through time algoirhtm (hyperparameter)
lr = 0.1 --learning rate

-- The matrix data is 
--[[    X      Y
     1,2,3,4 | 5
     5,6,7,8 | 9 
--]]

X = torch.Tensor{{1,2,3,4},{5,6,7,8}} -- if using one data point at a time use column vector? 
Y = torch.Tensor{{5},{9}} -- column vector of target output

-- a sample is a row vector. This is needed for model:forward(x)

-- define model
-- recurrent layer
local lstmLayer = nn.LSTM(4,4,rho) -- LSTM(input size, output size, rho) input size is the dimension of X and the output size is just 4
                           -- In this case each LSTM cell returns a value, but we can also set output size to one 

local model = nn.Sequential() -- this is where we add the neural netowrk on top of the LSTM layer
   :add(lstmLayer) --add recurrent/lstm layer
   
   -- here is where we add the standard feed forward neural net.
   :add(nn.Linear(4,3)) -- 3 hidden units from the single 4 outputs of lstm layer
   :add(nn.Sigmoid()) -- add sigmoid activaiton layer 
   :add(nn.Linear(3,1)) -- 'linear regression layer' with one output value
  
  -- structure is lstm -> 1 -> 3 hidden nodes w/ sigmoid -> one output node 

-- add sequencer
rnn = nn.Sequencer(model)
--set criterion (loss/energy)
criterion = nn.SequencerCriterion(nn.MSECriterion())

print("model:", rnn)

local iteration = 1
parameters,gradParameters = rnn:getParameters() -- parameters = weights and biases of neural net.
gradParameters:zero()	 -- used to store the gradient of weights/biases

-- SGD Optimization
while iteration<1000 do
  
  rnn:zeroGradParameters() -- torch accumulates (summation) of the gradient each time you call the model to compute the gradient. 
                           -- We don't want that so we just reset/zero out 
  i = torch.random(1,2) -- generate random number to select a random sample for SGD
  sX = X[i]:reshape(1,4) -- Equivlant to X[i] in numpy. This is the (sampled X) the 4 is the diminesion of the feature set of data. 
  sY = Y[i]:reshape(1,1) -- Equivlant to Y[i] in numpy. This is the sampled Y
  
  
  local outputs = rnn:forward(sX) -- prediciton
  local err = criterion:forward(outputs, sY) -- Squared error 
  local gradOutputs = criterion:backward(outputs, sY) 
  local gradInputs = rnn:backward(sX, gradOutputs)  -- true gradient
  delta = -lr*(gradParameters)
  parameters:add(delta) -- weight update..... W = W + delta 
    
  print('error = ', err)
  
  iteration = iteration + 1
  
end --end iteration



-- these are the predicted values afte the model has been trained

print('\n after model is trained the predicionts are:')
a = torch.Tensor{{1,2,3,4}}
print(rnn:forward(a))

b = torch.Tensor{{5,6,7,8}}
print(rnn:forward(b))



--print(rnn:forward(X)) for some reason using the whole matrix to make predictions gives different results than 
--using one sample at a time

