# Imports
#Set the logging of Tensorflow to (0) DEBUG [default], INFO, WARN, ERROR, FATAL (4)
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
# Place this before directly or indirectly importing tensorflow
#import logging
#logging.getLogger("tensorflow").setLevel(logging.WARNING)

from keras.models import Sequential,load_model
from keras.layers import Dense, Activation, Merge
from keras.layers.core import Masking
from keras.layers.wrappers import TimeDistributed
from keras.layers.recurrent import LSTM
from keras.utils.np_utils import to_categorical
from keras.engine import Layer
from keras.optimizers import SGD, RMSprop
from keras import backend as K
from keras.callbacks import TensorBoard
import copy
import numpy as np
import zipfile
import json
from keras.utils.layer_utils import layer_from_config
from tabulate import tabulate
import sys
from keras.layers.advanced_activations import LeakyReLU
from keras.utils.visualize_util import plot
from keras.activations import softmax as Ksoftmax
import tensorflow as tf

CONST_NUM_ACTIONS = 6 #go forward,left, right, grab, shoot, no op
CONST_NUM_OBSERVATIONS = 5 #bump,glitter,breeze,stench,scream
CONST_ACTUAL_INPUTS = CONST_NUM_OBSERVATIONS + 1 #add previous action
CONST_NOOP_ACTION = 5 #number for no-op action
CONST_MASK = 2.0


'''Calculate a random move and in a random number of turns
'''
def get_activations(model, layer, X_batch):
    #output = [model.layers[layer].get_output_at(ii) for ii in range(model.layers[layer].get_config()['output_dim'])]
    output = [model.layers[layer].output]
    get_act = K.function([model.layers[0].input], output) #also had: allow_input_downcast=True
    activations = get_act(X_batch) # same result as above
    return activations
def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()

#Randomly chooses and outputs 0,0,1,0,0,0, so then training is just....
#class randomActivation(Layer):
#    def __init__(self,**kwargs):
#        super(randomActivation, self).__init__(**kwargs)
#        self.supports_masking = True
#    def call(self, inputs, mask=None):
#        e = K.softmax(inputs)
#        #Problem is that multinomial is not differentiable
#        # There is an option to do out=tf.stop_gradient(input) to not calculate the derivative, and treat it like a constant
#        #Another option is to setup constant tensors that are already pre-randomized by sequence (so learning )
#        #indice = tf.multinomial(e,num_samples=1)
#        #indice = tf.squeeze(indice,0)
#        indice = [inputs[0]]
#        #out = inputs * (tf.one_hot(indice, CONST_NUM_ACTIONS) - 1.0)
#        out = tf.one_hot(indice, CONST_NUM_ACTIONS)
#        #out = tf.stop_gradient(out)
#        #out = inputs + out
#        return out
'''
NOTES:
Go back to individual updates of each output, so a fit can be narrowed only to a few outputs.
Problem I was having was saving the merge softmax.  But now that I do not care about that, and can compute it on my own,
it is ok.
'''

class myLL(Layer):
    '''Special version of a Rectified Linear Unit
     that allows a small gradient when the unit is not active:
     `f(x) = alpha * x for x < 0`,
     `f(x) = x for x >= 0`.

     # Input shape
         Arbitrary. Use the keyword argument `input_shape`
         (tuple of integers, does not include the samples axis)
         when using this layer as the first layer in a model.

     # Output shape
         Same shape as the input.

     # Arguments
         alpha: float >= 0. Negative slope coefficient.
    '''
    def __init__(self, alpha=0.3, **kwargs):
        self.supports_masking = True
        self.alpha = alpha
        super(myLL, self).__init__(**kwargs)
    def call(self, inputs, mask=None):
        return K.relu(inputs, alpha=self.alpha)

    def get_config(self):
        config = {'alpha': self.alpha}
        base_config = super(LeakyReLU, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

class mymodel:
    def __init__(self,config,filename=None,tolearn=True):
        '''
        If filename is not None then load the keras model.  Else make a new model.
        '''
        self._dolearn = tolearn
        self.setDefaults(config)
        all_models = []
        # Model
        h_layer_1_size = 10 #100 #this is the LSTM
        h_layer_2_size = 6 #this is the non-LSTM
        h_layer_3_size = 50 #this is the LSTM

        #Load file
        #TODO: Add loading by self._config['CONST_NORMAL']
        if filename:
            end_model = load_model(filename)
        else:

            def setupFirstLayer(the_model):
                if self._config['CONST_USE_LSTM']:
                    #Masks an input sequence by using a mask value to identify timesteps to be skipped.
                    the_model.add(Masking(mask_value=CONST_MASK, input_shape=(self._config['CONST_MAX_TIMESTEPS'],CONST_ACTUAL_INPUTS)))
                    layer1 = LSTM(output_dim=h_layer_1_size, init='glorot_uniform', activation=None, dropout_W=self._config['CONST_LSTM_DROP_INPUT'], dropout_U=self._config['CONST_LSTM_DROP_RNN'], W_regularizer=self._config['CONST_LSTM_INPUT_REGULARIZER'], U_regularizer=self._config['CONST_LSTM_RNN_REGULARIZER'])
                    the_model.add(layer1)


                else:
                    layer1 = Dense(output_dim=h_layer_2_size,input_dim=CONST_NUM_OBSERVATIONS,activation=None)
                    the_model.add(layer1)

            #Add activation
            if self._config['activation'] == 'leakyrelu':
                active = LeakyReLU(alpha=0.3)
            elif self._config['activation'] == 'relu':
                active = Activation('relu')
            elif self._config['activation'] == 'tanh':
                active = Activation('tanh')
            elif self._config['activation'] == 'softsign':
                active = Activation('softsign')
            else:
                active = Activation('softsign') #recommended by https://deeplearning4j.org/lstm for LSTM instead of Tanh; helps with saturation

            #layer2 = Dense(output_dim=h_layer_3_size,W_regularizer=self._config['CONST_DENSE_REGULARIZER'])
            #end_model.add(layer2)
            #end_model.add(Activation("softsign"))


            if self._config['CONST_NORMAL']:
                end_model = Sequential()
                setupFirstLayer(end_model)
                end_model.add(active)
                if self._config['CONST_DENSE_REGULARIZER'] != None:
                    layer3 = Dense(output_dim=CONST_NUM_ACTIONS,W_regularizer=self._config['CONST_DENSE_REGULARIZER'],activation=None)
                else:
                    layer3 = Dense(output_dim=CONST_NUM_ACTIONS,activation=None)


                end_model.add(layer3)
                end_model.add(Activation("softmax"))
                #end_model.add(randomActivation())
                #end_model.add(myLL(alpha=0.3))
                #end_model.add(LeakyReLU(alpha=0.3))
                end_model.compile(loss="categorical_crossentropy",optimizer=RMSprop(lr=self._config['CONST_LEARNING_RATE']),metrics=['accuracy'])

                self.end_model = end_model
            else:
                for _ in range(CONST_NUM_ACTIONS):
                    new_model = Sequential()
                    setupFirstLayer(new_model)
                    new_model.add(active)
                    if self._config['CONST_DENSE_REGULARIZER'] != None:
                        layer3 = Dense(output_dim=1,W_regularizer=self._config['CONST_DENSE_REGULARIZER'],activation=None)
                    else:
                        layer3 = Dense(output_dim=1,activation=None)
                    new_model.add(layer3)
                    #RMSprop
                    new_model.compile(loss="binary_crossentropy",optimizer=SGD(lr=self._config['CONST_LEARNING_RATE']),metrics=['accuracy'])

                    all_models.append(new_model)
                self.all_models = all_models



        self.distribution = None
        self._all_repredict = []
        self._weight_change = []
        self.reset()
        self.resetLearn()
        self._next_random_move = self.randMove()

    def setDefaults(self,config):
        self._config = {}
        self._config['LEARN_LAST_NUM'] = 5 #last number of moves to learn from
        self._config['CONST_OFFSET'] = 0.8 # was 0.8 Offset of the good or bad decision
        self._config['CONST_NUM_EPOCH'] = 1
        self._config['CONST_DISCOUNT_FACTOR'] = 0.7 #for crediting bad/good moves from the last move back to the first move
        self._config['CONST_LEARNING_RATE'] = 0.01 #0.001 is default
        self._config['CONST_USE_LSTM'] = False
        self._config['CONST_ENABLE_LOGGING'] = False #False #used to save more debug information for logging
        self._config['CONST_LOG_WEIGHTS'] = False # Add weights changes to the log file
        self._config['CONST_MAX_TIMESTEPS'] = 50 # number of timesteps - max number of timesteps the simulation can have
        self._config['CONST_LSTM_DROP_INPUT'] = 0.2
        self._config['CONST_LSTM_DROP_RNN'] = 0.2
        self._config['CONST_LSTM_INPUT_REGULARIZER'] = 'l2' #first layer
        self._config['CONST_LSTM_RNN_REGULARIZER'] = 'l2' #first layer
        self._config['CONST_DENSE_REGULARIZER'] = None#'l2' # second layer
        self._config['CONST_MAX_RANDOM_MOVE_TURNS'] = 5 #max number of turns between random moves
        self._config['CONST_MIN_RANDOM_MOVE_TURNS'] = 2 #min number of turns between random moves
        self._config['CONST_EXPLORE'] = False # should perform exploring
        self._config['activation'] = 'softsign' #'leakyrelu'
        self._config['CONST_NORMAL'] = False #True means to not use multi-models

        #overwrite
        for key in self._config:
            if key in config:
                self._config[key] = config[key]
        #nneds to be lowercase for comparison
        self._config['activation'] = self._config['activation'].lower()


    def reset(self):
        self._actions = []
        self._observations = []
        self._actions_dist = []
        self._repredict = []
        self._actions_num = []
        self._actions_single = []
        self._actions_scale = []
        self._x_test = []
        #self.model.reset_states() #reset LSTM state

    def resetLearn(self):
        if self._config['CONST_USE_LSTM']:
            self._all_learn_x = np.array([]).reshape( (0,self._config['CONST_MAX_TIMESTEPS'],CONST_ACTUAL_INPUTS) ) #shape needs to be same
            self._all_learn_y = np.array([]).reshape( (0,CONST_NUM_ACTIONS) ) #shape needs to be same
        else:
            self._all_learn_x = np.array([]).reshape( (0,CONST_NUM_OBSERVATIONS) ) #shape needs to be same
            self._all_learn_y = np.array([]).reshape( (0,CONST_NUM_ACTIONS) ) #shape needs to be same
            self._all_learn_x_num = [] #shape needs to be same



    def save(self,filename):
        if self._config['CONST_NORMAL']:
            self.end_model.save(filename)
        else:
            #TODO: Add save for self._config['CONST_NORMAL']
            pass

    def randMove(self):
        numMoves = np.random.random_integers(self._config['CONST_MIN_RANDOM_MOVE_TURNS'],high=self._config['CONST_MAX_RANDOM_MOVE_TURNS'])
        nextMove = np.random.random_integers(CONST_NUM_ACTIONS) - 1
        return {'numMoves':numMoves,'nextMove':nextMove,'moveCount':0}

    def predictAll(self, X_test):
        if self._config['CONST_NORMAL']:
            model = self.end_model
            proba = model.predict_proba(X_test, verbose=0)
            action_dist = proba[-1]
            return action_dist
        else:
            result = []
            for model in self.all_models:
                proba = model.predict_proba(X_test, verbose=0)
                result.append(proba[-1][0])
            npresult = np.array(result)
            return softmax(npresult)

    '''
    Predict the next action, given the new observation, and the previous action taken
    It will concat that observation onto the end of an array of all the observations across time up till now.
    Return the action to take, after performing a random action based on the distribution across actions the network gives.
    To get the distribution used, see self.distribution.
    '''
    def predict(self,obs):
        assert(len(obs) == CONST_NUM_OBSERVATIONS) #verify correct size of observations

        if self._config['CONST_USE_LSTM']:
            #Add previous action taken to Observation
            if len(self._actions_num) > 0:
                newobs = np.append(np.array(obs),self._actions_num[-1]*1.0/(CONST_NUM_ACTIONS-1)) #scale previous action between 0-1
            else:
                #no previous actions, so use NOOP
                newobs = np.append(np.array(obs),CONST_NOOP_ACTION*1.0/(CONST_NUM_ACTIONS-1)) #scale previous action between 0-1

            self._observations.append(newobs)
            X_test = np.array([np.array(self._observations)])#Extra array because it is one sample
            X_test = np.pad(X_test,((0,0),(0,self._config['CONST_MAX_TIMESTEPS']-X_test.shape[1]),(0,0)),'constant',constant_values=CONST_MASK)
        else:
            self._observations.append(obs)
            X_test = np.array([np.array(obs)])
        #x: input data, as a Numpy array or list of Numpy arrays (if the model has multiple inputs).
        #LSTM: Input Shape: (nb_samples, timesteps, input_dim)
        #Generates output predictions for the input samples, processing the samples in a batched way.
        action_dist = self.predictAll(X_test)
        #action_dist = softmax(action_dist)

        #Every semi-random number of moves make a uniform random choice
        if self._config['CONST_EXPLORE'] and self._dolearn and self._next_random_move['moveCount'] >= self._next_random_move['numMoves']:
            action = self._next_random_move['nextMove']
            self._next_random_move = self.randMove()
        else:
            if self._dolearn and self._config['CONST_EXPLORE']:
                self._next_random_move['moveCount'] += 1
            action = np.random.choice(np.arange(CONST_NUM_ACTIONS),p=action_dist) #randomly choose based on the distribution
            #action = 0
        #Save the array form of the action chosen too (e.g. [0,0,1,0,0,0])
        action_array = to_categorical([action], CONST_NUM_ACTIONS)
        #action_array = [action_dist]

        proba_scale = None #not used
        self.distribution = action_dist #temporary distribution for last action

        #Save for learning
        self._x_test.append(X_test[0]) #remove outer element
        self._repredict.append(action_dist) #save for analysis of change
        self._actions_dist.append(action_dist) #save distribution for all time steps
        self._actions_scale.append(proba_scale)
        self._actions_single.append(action_dist)
        self._actions.append(action_array[-1])
        self._actions_num.append(action)
        return action


    '''
    Uses the observations from a single game and fits the data.
    Adds the current scenario information to the training package
    '''
    def add_to_learning(self,score):
        #score range should -1100 to 1000; offset to -1050 to 1050, then scale to -1 to 1 by dividing by 1050
        #new_score = (score*1.0 + 50.0)/1150.0
        #The only reason a positive score happens is if the Gold is picked up.
        #  Otherwise between -100 and 0 is avoiding death.  Anything less is bad.  So maybe -20 is middle.  So offset
        #new_score = (score*1.0 + 50.0)/1150.0
        #new_score = (score*1.0)/1000.0
        if self._config['CONST_USE_LSTM']:
            X_train = self._x_test
        else:
            X_train = np.array(self._observations)
        #Modify the expected to be higher or lower for the decision chosen based on the score.
        num_updates = min(self._config['LEARN_LAST_NUM'],len(X_train))
        #simple +/- 1 score
        if score > 0:
            new_score = 1.0
        else:
            new_score = -1.0

        cur_dis_fact = 1.0 #start with full
        discounts = []
        for _ in range(num_updates):
            discounts.insert(0,cur_dis_fact)
            cur_dis_fact *= self._config['CONST_DISCOUNT_FACTOR'] #make it progressively smaller changes back in time
        tmp0 = np.array(discounts) * self._config['CONST_OFFSET'] * new_score
        #
        #tmp2 = np.array(self._actions[-num_updates:]).T * tmp0 #change from vector to matrix of all actions
        #Y_train = np.array(self._actions_single[-num_updates:]) +  tmp2.T #matrix additions


        #make everything 0 except the one value that is not 0, and make that 1
        values = np.abs(np.array(self._actions[-num_updates:]))
        #set_values = values/np.max(values)
        Y_train = (values.T * tmp0).T
        #tmp0 = np.zeros(num_updates)+new_score

        #Shrink down to what is selected to update
        X_train = X_train[-num_updates:]
        X_action = np.array(self._actions_num[-num_updates:]).astype(int)

        #add padding of CONST_MASK to the observations for every timestep that is blank
        #for idx in range(num_updates):
        #    tmp1 = np.array(X_train[idx])
        #    X_train[idx] = np.pad(tmp1,((0,self._config['CONST_MAX_TIMESTEPS']-tmp1.shape[0]),(0,0)),'constant',constant_values=CONST_MASK)
        X_train = np.array(X_train)

        self._all_learn_x = np.append(self._all_learn_x,X_train,axis=0)
        self._all_learn_x_num.append(X_action)
        self._all_learn_y = np.append(self._all_learn_y,Y_train,axis=0)
        if self._config['CONST_ENABLE_LOGGING']:
            layer1val = self.getLayer1Value([self._x_test])
            #self._all_repredict.append( (self._x_test,self._actions,self._repredict, tmp0, layer1val[-1]) )
            self._all_repredict.append( (self._x_test,self._actions,self._actions_single, tmp0, layer1val) )

    def getLayer1Value(self,X_test):
        if self._config['CONST_NORMAL']:
            layer1val = get_activations(self.end_model,0,X_test)
        else:
            #Every model's first layer is the same
            layer1val = get_activations(self.all_models[0],0,X_test)
        return layer1val[-1]


    def get_weights(self):
        if self._config['CONST_NORMAL']:
            wwb = np.array(copy.deepcopy(self.end_model.get_weights())) #need to copy or will jusr point to updated weights
        else:
            wwb = np.array(copy.deepcopy(self.all_models[0].get_weights())) #need to copy or will jusr point to updated weights
            for idx in range(CONST_NUM_ACTIONS-1):
                wwb2 = np.array(copy.deepcopy(self.all_models[idx+1].get_weights())) #need to copy or will jusr point to updated weights
                wwb[2] = np.append(wwb[2],wwb2[2],axis=1)
                wwb[3] = np.append(wwb[3],wwb2[3],axis=0)
        return wwb


    '''
    Learn from all the scenarios saved.
    '''
    def learn(self):
        #only last 10 moves before end
        #NOTE: Issue with doing the update separate, because the network changes after each update, so the Y_train will not be set to 0 delta (only minor updates)
        if self._config['CONST_ENABLE_LOGGING'] and self._config['CONST_LOG_WEIGHTS']:
            self._weight_change = []
            chg_loc_all = []
            wwb = self.get_weights()
        #LSTM: Input Shape: (nb_samples, timesteps, input_dim) - (1,1,5) ; (1,2,5) ; (1,3,5)
        #LSTM: Labels Shape (nb_samples, input_dim) - (1,1) ; (1,1) ; (1,1)
        #x: input data, as a Numpy array or list of Numpy arrays (if the model has multiple inputs).
        #y: labels, as a Numpy array.
        #have to cycle through because np.array has to be the same length for all elements, but since the timesteps are different each time, it has to be trained separately
        #Used padding so all X_train are the same size of timesteps, so can be just one large fit function.
        #self.end_model.fit(X_train, Y_train, nb_epoch=self._config['CONST_NUM_EPOCH'], verbose=0,
        #    callbacks=[TensorBoard(log_dir='./logs',histogram_freq=1, write_graph=True)])
        if self._config['CONST_NORMAL']:
            X_train = self._all_learn_x
            Y_train = self._all_learn_y
            self.end_model.fit(X_train, Y_train, nb_epoch=self._config['CONST_NUM_EPOCH'], verbose=0)
        else:
            idx = -1
            for action in self._all_learn_x_num:
                idx+=1
                for act in action:
                    X_train = np.array([self._all_learn_x[idx]])
                    Y_train = np.array([self._all_learn_y[idx][act]])
                    self.all_models[act].fit(X_train, Y_train, nb_epoch=self._config['CONST_NUM_EPOCH'], verbose=0)

        sys.stdout.write(" ...learned")
        sys.stdout.flush()

        if self._config['CONST_ENABLE_LOGGING'] and self._config['CONST_LOG_WEIGHTS']:
            wwa = self.get_weights()
            #wwd = np.array(wwa) - np.array(wwb)
            #chg_loc_one = []
            #chg_val_one = []
            #for jj in range(len(wwd)):
            #    chg_loc = np.nonzero(wwd[jj])
            #    if len(chg_loc[0]) > 0:
            #        chg_loc_one.append( (jj,chg_loc) )
            #        chg_val_one.append(wwd[jj][chg_loc])
            #if len(chg_loc_one) == 0:
            #    chg_loc_all.append(("None","None"))
            #else:
            #    chg_loc_all.append( (chg_loc_one,chg_val_one) )

            #self._weight_change.append(chg_loc_all)
            self._weight_change = (wwb,wwa)
        self.resetLearn()


    '''
    Compare previous prediction before learning with new prediction after learning.
    self._observations is the X_test and the Y_test is the self._repredict
    Save to fobj
    '''
    def learn_analysis(self,fobj):
        print("Obs(bump,glitter,breeze,stench,scream)      Action(forward,left,right,grab,shoot,noop)     Action Prob Diff after learning     Delta Loss",file=fobj)
        idx = -1
        if self._config['CONST_LOG_WEIGHTS']:
            fobj2 = fobj #temp to keep same
            (wwb,wwa) = self._weight_change
        for (observations,actions,repredict,change, oldlayer1) in self._all_repredict:
            idx += 1
            cur_num = -1
            startChange = len(repredict)-len(change)
            for Y_test in repredict:
                cur_num += 1
                x_test = np.array([observations[cur_num]])
                proba = self.predictAll(x_test)
                newlayer1 = self.getLayer1Value([x_test])
                Y_test_new = proba
                Y_test_new2 = softmax(Y_test_new) #Apply Softmax
                Y_test2 = softmax(Y_test) #Apply Softmax
                chg = Y_test_new2 - Y_test2#show change in probabilities
                #Start showing change when there are changes
                if cur_num >= startChange:
                    outchange = change[cur_num-startChange] #works because there is only one update
                else:
                    outchange = "" #no change
                #make a nice string for the action distribution
                chgstr = "[ %.4f  %.4f  %.4f  %.2f  %.4f  %.4f ]" % (chg[0],chg[1],chg[2],chg[3],chg[4],chg[5])
                if self._config['CONST_USE_LSTM']:
                    obs = observations[cur_num][cur_num]
                    obs[-1] = obs[-1] * (CONST_NUM_ACTIONS-1) #change back to 0-5
                else:
                    obs = observations[cur_num]
                print(obs,actions[cur_num],chgstr,outchange,file=fobj)
                #Start showing change when there are changes
                if cur_num >= startChange:
                    if self._config['CONST_LOG_WEIGHTS']:
                        print("Old weights:",file=fobj)
                        self.print_weights(fobj2,w=wwb,hasWeights=True,hasInput=True,inp=[x_test[0],oldlayer1[cur_num],Y_test])
                        print("New weights:",file=fobj)
                        self.print_weights(fobj2,w=wwa,hasWeights=True,hasInput=True,inp=[x_test[0],newlayer1[-1],Y_test_new])


            print("",file=fobj)
        self._all_repredict = [] #delete what we analyzed

        if self._config['CONST_LOG_WEIGHTS']:
            print("Weight change",file=fobj2)
            self.print_weights(fobj2,w=wwb,d=(wwa-wwb),hasWeights=True,hasDelta=True)
            #print("Final weights",file=fobj2)
            #self.print_weights(fobj2)
            self._weight_change = []

    def print_weights(self,fobj,w=None,hasWeights=False,d=None,hasDelta=False,hasInput=False,inp=None):
        if not hasWeights:
            w = self.get_weights()
        #print(w)
        isbias = False
        for ii in range(len(w)):
            layernum = int((ii+1)/2)
            if not isbias:
                layernum += 1
            cur = w[ii]
            if hasDelta:
                dcur = d[ii]
            if cur.ndim == 1:
                cur = cur.reshape((1,cur.shape[0]))
                if hasDelta:
                    dcur = dcur.reshape((1,dcur.shape[0]))
            if not isbias:
                layer = []
                headerrow = False
            for jj in range(len(cur)):
                op = []
                if isbias:
                    op.append('b')
                else:
                    if hasInput:
                        ss = "In %.5f"%inp[layernum-1][jj]
                    else:
                        ss = "In %d"%(jj+1)
                    op.append(ss)
                lay = cur[jj]
                if not headerrow:
                    if hasInput:
                        preop = ['Out %.5f'%aa for aa in inp[layernum]] #layernum is always +1
                    else:
                        preop = ['Out %d'%aa for aa in range(1,len(lay)+1)]
                    preop.insert(0,"")
                    layer.append(preop)
                    headerrow = True
                for kk in range(len(lay)):
                    ss = '%3.5f'%lay[kk]
                    if hasDelta:
                        ss += ' (%3.5f)'%dcur[jj][kk]
                    op.append(ss)
                layer.append(op)
            if isbias:
                print("Layer %d"%layernum,file=fobj)
                print(tabulate(layer),file=fobj)
                print("",file=fobj)
            isbias = not isbias #toggle
        #plot(self.end_model, to_file=fname, show_shapes=True)


    def isLogging(self):
        return self._config['CONST_ENABLE_LOGGING']

    '''
    Prints information about the neural network probablity distribution given a
    '''
    def net_info(self):
        import sys
        import os

        def getname_percept(idx):
            if idx == 0:
                return "bump"
            elif idx == 1:
                return "glitter"
            elif idx == 2:
                return "breeze"
            elif idx == 3:
                return "stench"
            elif idx == 4:
                return "scream"
        def getname_action(idx):
            if idx == 0:
                return "FOWARD"
            elif idx == 1:
                return "RIGHT"
            elif idx == 2:
                return "LEFT"
            elif idx == 3:
                return "GRAB"
            elif idx == 4:
                return "SHOOT"
            elif idx == 5:
                return "NOOP"

        #cycle through all combinations
        #make tabulate
        num_neurons = 5
        num_actions = 1
        last_row = num_neurons
        #Setup table:
        # bump      0   1   0   1   ...
        # glitter   0   0   1   1   ...
        # ...
        # ACTION    FORWARD
        multi_action = True #If only one action, or random action
        if multi_action:
            num_actions = 6
        tbl = [ [ 0 for _ in range(2**num_neurons+1) ] for _ in range(num_neurons+num_actions)]#  [[0.0]*(len(neurons)+1)]*(len(neurons))
        for ii in range(num_neurons):
            tbl[ii][0] = getname_percept(ii)
        if multi_action:
            for ii in range(num_actions):
                tbl[last_row+ii][0] = getname_action(ii)
        else:
            tbl[last_row][0] = "ACTION"

        for idx_col in range(2**num_neurons):
            inpercepts = idx_col
            true_idx_col = idx_col + 1
            nnpercept = [] #bump,glitter,breeze,stench,scream
            for ii in range(5):
                val = ( inpercepts & (2**ii) ) / (2**ii)
                nnpercept.append( val )
                tbl[ii][true_idx_col] = int(val)
            nnpercept = np.array(nnpercept)
            self.reset() #clean out previous observations
            action = self.predict(nnpercept)
            if multi_action:
                waction = self.distribution
                for ii in range(num_actions):
                    tbl[last_row+ii][true_idx_col] = ('%0.5f' % (waction[ii]))
            else:
                tbl[last_row][true_idx_col] = getname_action(action)

        #print("Weights for connection from Neuron on Row --> Neuron on Column\n")
        return tabulate(tbl)


#See https://keras.io/layers/recurrent/


'''
So the idea:
Do policy roll-outs of 200 games with the network, using predict_proba with
    X_test = [
                 [ 0 0 1 0 0], [ 0, 1 ,0, 1, 1 ]
             ]
    making it 2D but with only a sequece of the current game observations added into one element, because we only have one game
Then take the game score and use some equation to determine if it was a good or bad game (in proportions).
For each game, use the sequence of probabilities and multiply the highest by a proportion based on if good or bad (i.e. increase if good, decrease if bad) and rescale with softmax and make that the Y_label.
Then do a fit with the X_train = [ [[0,0,1,0,0], [0,1,0,0,0]],[0,1,1,0,0],[0,1,1,0,0]]  3D because all games will be in there.

Still trying to figure out the TimeDistributed() if it is needed.  Do not feel the LSTM(stateful=True) is needed because I am sending the entire sequece each time, so no memory needed.
The loss should probably be log loss??
'''
